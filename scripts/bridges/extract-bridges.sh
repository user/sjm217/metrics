#!/bin/bash
# Extract contents of archives (e.g., from-tonga-2008-05-16T194647Z.tar.gz)
# to separate subdirectories in newly created directory data
# (e.g., data/2008-05-16T194647Z)
for i in `ls bridges/ | cut -c 12-29`
do
mkdir "data/bridges/"$i
tar -C "data/bridges/$i" -xf "bridges/from-tonga-"$i".tar.gz"
done

