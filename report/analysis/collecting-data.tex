\documentclass[a4paper]{article}
\usepackage[dvips]{graphicx}
\usepackage{graphics}
\usepackage{color}
\begin{document}
\title{Collecting Statistical Data in the Tor Network}
\author{Karsten Loesing}
\maketitle

\section{Introduction}

% This paper wants two things at once:
% 1. Answer all concerns about "collecting data in an anonymity network" before people have the opportunity to express them.
% 2. Describe what data could be useful and how it would be collected from an engineering POV.

The public Tor network is an overlay network consisting of a little more than one thousand servers, called relays, and probably hundreds of thousands of clients.
Anyone with a moderately fast Internet connection can set up a relay, and everyone can join the network as a client.
Relays are pushing a few hundred megabytes of data per second on behalf of clients with the main goal of separating origins from contents of transported data.
% TODO could cite Tor design paper
Figure~\ref{fig:tornetwork} shows a high-level overview of the Tor network.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{tornetwork}
\caption{Overview of the Tor network}
\label{fig:tornetwork}
\end{figure}

A few facts are known about the public Tor network, even though single individuals or organizations, including The Tor Project, control only small parts of the network.
The reason is that clients need to know which relays are in the network to build circuits and anonymize their traffic.
Therefore, relays periodically report their addresses and capabilities to a small set of directory servers which redistribute relay lists to clients.
One can collect these lists over time and analyze them to obtain statistics about the relays in the Tor network.
Further, relays include highly aggregated data about transported traffic which can be used to estimate usage of the Tor network.
But this concludes the available data about the Tor network.
In particular, there is little known about usage of the Tor network including numbers and origins of clients.

The purpose of this paper is to discuss data collection in the Tor network for statistical purposes.
There are a few questions that this paper focuses on:
\begin{itemize}
\item \emph{Why} is it necessary to collect and evaluate data in the Tor network? (Section~\ref{sec:purposes})
\item \emph{How} can these data be collected without putting the users at risk? (Section~\ref{sec:aggregation})
\item \emph{What} data in detail are useful to collect? (Sections~\ref{sec:publicdata}--\ref{sec:privatedata}, ordered by sensitivity of collected data)
\end{itemize}
This paper makes no claim of giving definite answers to these questions.
It is rather meant as an attempt to start the discussion within the Tor community.

\newpage

\section{Purposes of collecting data}
\label{sec:purposes}

Collecting statistical data should always serve a purpose, besides printing pretty graphs.
In the given case of an anonymizing network, there should even be a real need for collecting data, as there is an inherent sensitivity of collecting data.
There are a couple of reasons why statistics about the Tor network are needed:

\begin{enumerate}
\item \emph{Performance metrics:} Tor is slow.
Performance metrics can help in understanding why this is the case.
These data shall further help in learning whether any changes to the Tor design actually improve performance.
%TODO could cite Feb report on Evaluation of Relays from Public Directory Data

\item \emph{User metrics:} The Tor Project aims at providing anonymity and censorship circumvention to people worldwide.
In particular, the goal is to make Tor more useful for people in various target countries around the world.
User metrics can help in detecting which of these countries are succeeding and which ones need more work.

\item \emph{Blocking-resistance metrics:} Right now there is no reliable way to tell if countries are trying to block users from reaching the Tor network.
A few people report such events, but that is neither very quick nor reliable.
Metrics about Tor usage can automatically detect country-wide blocking events.

% TODO briefly thought about another bullet point:
%
%Usage metrics. What percentage of traffic is file sharing, what
%percentage web browsing, what percentage AIM, etc? Tracking these stats
%over time will let us know how we're doing -- if file sharing keeps going
%up and web browsing keeps going down, we need to do something about that.
%
%But: Do we really want to collect data about the type of traffic? That
%would require us to look into the exit traffic, which exceeds looking at
%the control commands to determine the countries and ports where we exit.
%I mean, I can see why these data would be useful, but I also imagine
%there could be legal issues.

\item \emph{Metrics tools:} Some organizations are interested in learning how many of their visitors access their web servers over Tor.
In those cases, the data are collected at the organizations' servers.
Metrics tools can help in evaluating which of their visitors are Tor users on which are not.
\end{enumerate}

\section{Aggregating sensitive data}
\label{sec:aggregation}

Before going into the details of data collection, it is important to think about sensitivity of the data and how to collect them safely.
The most sensitive data are those that can be related to either a \emph{person} or to a specific \emph{action}.
Personal data include IP addresses and all information that can be derived from IP addresses such as the geolocation on city or country level.
Actions comprise requests and responses including their content, application protocol, server address, and server port.
Any data collection must ensure that neither collected IP addresses nor specific actions can be misused to deanonymize users.

Fortunately, statistical evaluations do not require single observations but only aggregated data to discover trends.
It does not matter when the data are aggregated, so that this step can be performed already during data collection.
When potentially sensitive data are processed directly at their source, they cannot leak and be used to deanonymize users.
The only requirement is that the aggregated and therefore less sensitive data are safe from being misused.
The aggregated data can then be transferred over a possibly untrusted network, collected at a central place, and made publicly available.

Some data do not require any aggregation, but most data should be aggregated at their source.
Depending on the sensitivity of data, there are multiple ways to aggregate data:

\begin{enumerate}
\item IP addresses can be resolved to cities or countries using a \emph{GeoIP database}.
For statistical purposes it is sufficient to make statements about users in certain countries rather than considering single users.
A negative effect of this (often inevitable) aggregation is that precision depends on the used GeoIP database (which might be outdated).
Further, the information whether two distinct requests from the same country originate from the same IP address gets lost.
If this information is relevant for an evaluation, the source needs to memorize IP addresses until the aggregation step can take place.

\item Requests exiting the network can be resolved to countries of destination servers or to requested ports.
Another approach is to look at the transported data and resolve requests to application protocols.
This approach may be more problematic, though, because it involves reading traffic content as opposed to envelope data.

\item Rather than recording timestamps of single events, the number of events in a given \emph{time frame} can be counted.
Unless the precision of the later analysis should be shorter than the aggregation time, the data loss by this aggregation is negligible.

\item The exact number of events can be concealed by introducing \emph{artificial imprecision}.
One way to achieve this is to round up occurrences to the next multiple of some fixed number.
This approach has drawbacks if small numbers of observations are collected from a large number of sources.
The result will be lower precision of statistical results.
\end{enumerate}

\section{Collecting routing and performance data}
\label{sec:publicdata}

The first kind of data in the Tor network that is worth collecting for statistical analysis are routing and performance data.
Both data are non-sensitive in the sense that they cannot be used to deanonymize users.
These data do not require aggregation of any kind besides reducing the amounts of data to transport and store.
Everyone could collect these data today with minimal efforts, so that this data collection can as well be done at a central place and for public use.

\paragraph{Routing data}

Relays periodically upload router descriptors to the directory servers, advertising their IP address, exit policy, and Tor version.
The directory servers collect these router descriptors and vote on a list of available relays, called network status, every hour.
Both network status and router descriptors are downloaded by clients to select relays and to build circuits.
If someone is interested in collecting routing data over time, they can easily download network statuses and router descriptors using the same HTTP interface as clients do.
The Tor Project already collects these data since late 2005.

\paragraph{Performance data}

In addition to router descriptors advertising their capabilities, relays further upload extra-info documents containing data about usage by clients.
These documents contain histories of the number of bytes that a relay has read or written in 15-minutes intervals over the past 24 hours.
Clients do not need these histories for operation, so that the only purpose of extra-info documents is to be used for statistical analysis.
Extra-info documents are already collected by The Tor Project along with network statuses and router descriptors.

In addition to bandwidth histories, relays could collect more performance data about their operation and include them in extra-info documents.
For example, relays could passively monitor latencies and throughput of direct connections to other relays.
These data can help getting a better picture on the real capabilities of relays rather than the self-advertised bandwidth.
Researchers in the past have modified relays to lie about their real bandwidth and attract more clients which could be detected from reports of directly connected relays.
In the future, these data might also be passed on by the directories to clients to make better path-selection decisions.

Similarly, clients could passively monitor performance of the relays they have selected for circuits.
This monitoring includes both entry nodes to which they connect directly and middle or exit nodes to which they extend circuits subsequently.
Clients could use these data to adjust path selection and circuit build timeouts for future circuits.
% TODO see proposal 151

An alternative to passively monitoring performance on relays is to set up Tor clients that actively build circuits and send test data over them.
The Torflow library can be used to measure circuit build times and download times.
The advantage over passive monitoring is that these active measurements can be performed at a single place in the network which is easier to implement.
The disadvantage, however, is that measurements generate additional traffic for the network.
These measurements should be performed regularly on a few nodes only and be made public afterwards.

\section{Collecting possibly sensitive data}

The second kind of data that could be collected in the Tor network is more sensitive than the first.
The data described here is related to either client IP addresses or to requests exiting the Tor network.
Anyone running a relay in the network would be able to gather these sensitive data and leak them, and there is no way to stop them.
The approach taken here would be to aggregate the collected data at their sources to minimize or eliminate the potential for misuse.
The collection and aggregation process would be documented and implemented in the Tor source code.
The collected aggregated data would be made publicly available to be as transparent as possible.

\paragraph{Directory requests}

Every Tor client needs to download directory information from the directory servers, which can be either directory authorities or directory mirrors.
Directory downloads are less sensitive than application requests.
The only information that could leak is that someone is using Tor, not which actions they perform.
Client requests to the directory authorities and caches can help in getting an overview of Tor users.
From the requests, one can determine the clients' IP addresses and therefore countries from which they connect.
Further, the requested directory version (protocol version 2 or 3) allows to conclude which Tor series clients are using (0.0.x/0.1.x or 0.2.x).
Clearly, these data need to be handled with care.
Requests should be anonymized by resolving IP addresses to countries, collecting requests over a time of 4 hours, and rounding up request numbers to multiples of 4.
The aggregated client request data could be included in extra-info documents that directory mirrors publish to the directory authorities.

\paragraph{Entry guard clients}

Clients select a small number of relays as their entry guards and use them as first nodes in all their circuits.
Hence, entry guards learn about the clients' IP addresses, but not what actions they perform over the Tor network.
The requests seen at entry guards are even more sensitive than requests to directories, because they might be linked to certain actions that users perform from timing information.
It is, however, subject to discussion whether highly aggregated client data still pose a risk of possible deanonymization.
A useful aggregation would include resolving IP addresses to countries, summing up requests over a time of 48 hours, and rounding up to multiples of 8.
Client IP addresses should never be stored to disk, but only kept in memory to count unique IP addresses.
The results could be included in extra-info documents.

\paragraph{Bridge clients}

Similar to directories and entry guards, bridges could record how many clients from which countries connect to them in a given time frame.
Clients connect directly to bridges to use them as first relay in all circuits and to request directory information.
Similar to entry guards, raw client requests might be linked to specific actions, so that these data need to be highly aggregated.
It appears useful to apply the same aggregation as proposed for entry guard clients, that is, per-country aggregation over a time of 48 hours, rounding up to multiples of 8.
Bridges would include these data in the extra-info documents that they upload to the bridge authority.
However, the bridge authority would have to remove bridge contact information before making these data public.

\paragraph{Exit traffic}

Besides of client IP address, the usage of the Tor network in terms of exit traffic could be subject to statistical analysis.
Of course, exit traffic is at least as sensitive as client IP addresses, so that data should only be stored in highly aggregated form.
Two possible approaches are aggregation by countries of exit destinations and by destination ports.
An even more sensitive approach would be to resolve requests to the transported application protocol by looking at message headers.
Furthermore, requests should be collected over a time of 48 hours.
Rather than counting and possibly rounding up request numbers, the transported bandwidth should be summed up.

\section{Collecting non-public data}
\label{sec:privatedata}

The third kind of data is non-public in the sense that it can only be collected on servers run by a few individuals and The Tor Project.
These servers include the central directory authorities and the various web servers.
The data that could be collected on these servers are very sensitive and need to be aggregated during collection.
Before actually collecting these data, users need to be informed about the collection by adding some kind of website data collection policy.
The results of the statistical analysis would be made publicly available.

\paragraph{Bridge locations}

Like normal relays, bridges also upload descriptors describing their location and capabilities in order to be used by clients.
They send their descriptors to the bridge authority rather than to the directory servers.
Bridge locations are only given out in small contingents to clients that specifically ask for them.
The list of bridge addresses is very sensitive, not only to protect the fact that users access or have accessed Tor using bridges, but also to retain the function of bridges to allow uncensored access.
The bridge authority could resolve bridge IP addresses to countries and publish daily summaries.

\paragraph{Client requests to bridge database}

The origins and frequencies of clients trying to learn about new bridges can be important to determine typical bridge usage.
The bridge database, or rather the various interfaces to it (e-mail, website), see the IP addresses of bridge users.
These data should be aggregated by countries for a time frame of 48 hours, and rounded up to the next multiple of~8.

\paragraph{Downloading Tor}

Requests to the Tor website for downloading Tor can help get a good picture of origins of Tor users and update behavior (at least until the updater is in place and wide use).
Requests should be counted by requested Tor versions and platform.
These data should be aggregated by resolving IP addresses to countries over a time of 48 hours, rounding up to multiples of 4.
Likewise, the number of downloads from the various interfaces to gettor (e-mail, IRC) can be collected.
Once the Tor updater is in place, it can collect data about the number of
downloads and the requested versions.

\section{Conclusion}

The purpose of this paper is to start a discussion on collecting statistical data in the Tor network.
The paper has first motivated why it is necessary to collect data.
Reasons include better understanding of performance issues, measure community outreach, detect country-wide blocking events, and provide tools to third parties to determine how many of their visitors are Tor users.
The paper has further discussed approaches to aggregate sensitive data, so that they cannot be used to deanonymize users.
These approaches include resolving IP addresses to countries, exiting traffic to destination country or port number, counting events in time frames of at least a few hours, and introducing artificial imprecision by rounding up to the next multiple of a fixed number.
Finally, the paper has presented three kinds of data, ordered by sensitivity and potential to deanonymize users.
These data include routing and performance data which are not sensitive at all, more sensitive data that anyone in the Tor network could already collect today, and non-public data that would be collected on servers run by a few individuals and The Tor Project.
These data can help answer questions about the Tor network that cannot be answered so far.
Hopefully, the discussion results in a solid plan which data needs to be collected to answer these questions without putting the security of Tor users at risk.

\end{document}

