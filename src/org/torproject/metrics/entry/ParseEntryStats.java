/* Copyright 2009 Karsten Loesing
 * See LICENSE for licensing information */
package org.torproject.metrics.entry;

import java.io.*;
import java.text.*;
import java.util.*;

public final class ParseEntryStats {

    private static class DataPoint {
        String date;
        SortedMap<String, Integer> ips;
    }

    private static SortedSet<String> allCountries = new TreeSet<String>();
    private static SortedSet<String> allDates = new TreeSet<String>();
    private static SortedMap<String, SortedMap<String, DataPoint>> allDataPoints
            = new TreeMap<String, SortedMap<String, DataPoint>>();

    private static SortedMap<String, Integer> parseCountryLine(String line) {
        SortedMap<String, Integer> result = new TreeMap<String, Integer>();
        if (line.length() < 2 || line.split(" ").length < 2) {
            return result;
        }
        String[] countries = line.split(" ")[1].split(",");
        for (String part : countries) {
            String country = part.split("=")[0];
            Integer count = Integer.parseInt(part.split("=")[1]) - 4;
            allCountries.add(country);
            result.put(country, count);
        }
        return result;
    }

    private ParseEntryStats() {
    }

    public static void main(final String[] args) throws Exception {

        // check input parameters
        if (args.length < 2) {
            System.err.println("Usage: java "
                    + ParseEntryStats.class.getSimpleName()
                    + " <input directory> <output directory>");
            System.exit(1);
        }
        File inputDirectory = new File(args[0]);
        if (!inputDirectory.exists() || !inputDirectory.isDirectory()) {
            System.err.println("Input directory '"
                    + inputDirectory.getAbsolutePath()
                    + "' does not exist or is not a directory.");
            System.exit(1);
        }
        File outputDirectory = new File(args[1]);
        if (outputDirectory.exists() && !outputDirectory.isDirectory()) {
            System.err.println("Output directory '"
                    + outputDirectory.getAbsolutePath()
                    + "' exists, but is not a directory.");
            System.exit(1);
        }
        outputDirectory.mkdir();

        long started = System.currentTimeMillis();

        // parse input files
        for (File inputFile : inputDirectory.listFiles()) {
            SortedMap<String, DataPoint> currentDataPoints
                    = new TreeMap<String, DataPoint>();
            allDataPoints.put(inputFile.getName(), currentDataPoints);
            BufferedReader br = new BufferedReader(new FileReader(
                    inputFile));
            String line = null;
            String currentDate = null;
            DataPoint currentDataPoint = null;
            boolean haveSeenActualNumbers = false;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("written ")) {
                    if (haveSeenActualNumbers) {
                        currentDataPoints.put(currentDate, currentDataPoint);
                    }
                    currentDataPoint = new DataPoint();
                    currentDate = line.split(" ")[1];
                    allDates.add(currentDate);
                } else if (line.startsWith("started-at ")) {
                    // ignored
                } else if (line.startsWith("ips ")) {
                    currentDataPoint.ips = parseCountryLine(line);
                    if (line.split(" ").length > 1) {
                        haveSeenActualNumbers = true;
                    }
                }
            }
            if (haveSeenActualNumbers) {
                currentDataPoints.put(currentDate, currentDataPoint);
            }
            br.close();
        }

        System.out.printf("We have seen %d countries on %d days on %d "
                + "entry nodes.%n", allCountries.size(), allDates.size(),
                allDataPoints.size());

        for (Map.Entry<String, SortedMap<String, DataPoint>> e
                : allDataPoints.entrySet()) {
            String directory = e.getKey();
            SortedMap<String, DataPoint> dataPoints = e.getValue();
            File outFile = new File(outputDirectory.getAbsolutePath()
                    + File.separatorChar + directory + ".csv");
            BufferedWriter out = new BufferedWriter(new FileWriter(
                    outFile, false));
            out.write("time,");
            for (String f : allCountries) {
                out.write(String.format("ips%s,", f));
            }
            out.write("ipstotal\n");
            for (String date : allDates) {
                if (!dataPoints.containsKey(date)) {
                    out.write(date + ",");
                    int nas = allCountries.size();
                    for (int i = 0; i < nas; i++) {
                        out.write("NA,");
                    }
                    out.write("NA\n");
                } else {
                    DataPoint currentDataPoint = dataPoints.get(date);
                    out.write(date + ",");
                    int ipstotal = 0;
                    for (String f : allCountries) {
                        int ips = currentDataPoint.ips.containsKey(f)
                                ? currentDataPoint.ips.get(f) : 0;
                        ipstotal += ips;
                        out.write(String.format("%d,", ips));
                    }
                    out.write(String.format("%d%n", ipstotal));
                }
            }
            out.close();
        }

        System.out.println("Parsing finished after "
            + ((System.currentTimeMillis() - started) / 1000)
            + " seconds.");
    }
}

