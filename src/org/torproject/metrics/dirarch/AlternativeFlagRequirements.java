/* Copyright 2009 Karsten Loesing
 * See LICENSE for licensing information */
package org.torproject.metrics.dirarch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class AlternativeFlagRequirements {

    private static final long TWELVE_HOURS = 12L * 60L * 60L * 1000L;
    private static final long TWENTY_FOUR_HOURS = 24L * 60L * 60L * 1000L;

    private static class RelayHistory {

        // Data from current consensus; only used for one evaluation
        boolean exit;
        long advertisedBandwidth;
        boolean unstableVersion;
        double mtbf;

        // WFU calculation (Weighted Fractional Uptime)
        double totalUptime; // gesamtdauer uptime
        double totalTimeKnown; // muss auch discounted werden..

        // MTBF calculation (Mean Time Between Failures)
        double uptimeLength; // gesamtlänge abgeschlossene uptime-sessions
        double uptimeNumbers; // anzahl abgeschlossene uptime-sessions
        long currentSessionStart; // wann wurde aktuelle session gestartet
        long lastUptime; // letzter uptime wert
    }

    private static long percentile(List<Long> values, double percent) {
        List<Long> sorted = new ArrayList<Long>(values);
        Collections.sort(sorted);
        int length = sorted.size();
        double position = percent * ((double) length + 1.0) / 100.0;
        double floorpos = Math.floor(position);
        if (position < 1) {
            return sorted.get(0);
        }
        if (position >= (double) length) {
            return sorted.get(length - 1);
        }
        long lower = sorted.get(((int) floorpos) - 1);
        long upper = sorted.get((int) floorpos);
        return lower + (long) ((position - floorpos) * (double) (upper - lower));
    }

    private static double percentile(List<Double> values, double percent) {
        List<Double> sorted = new ArrayList<Double>(values);
        Collections.sort(sorted);
        int length = sorted.size();
        double position = percent * ((double) length + 1.0) / 100.0;
        double floorpos = Math.floor(position);
        if (position < 1) {
            return sorted.get(0);
        }
        if (position >= (double) length) {
            return sorted.get(length - 1);
        }
        double lower = sorted.get(((int) floorpos) - 1);
        double upper = sorted.get((int) floorpos);
        return lower + ((position - floorpos) * (double) (upper - lower));
    }

    private AlternativeFlagRequirements() {
    }

    public static void main(final String[] args) throws Exception {
        if (args.length < 3) {
            System.err.println("Usage: java "
                    + AlternativeFlagRequirements.class.getSimpleName()
                    + " [--create-evaluation-table] <database name> "
                    + "<database password> <output directory> ");
            System.exit(1);
        }
        int createTable = args[0].equals("--create-evaluation-table") ? 1 : 0;
        File outputDirectory = new File(args[createTable + 2]);
        if (outputDirectory.exists() && !outputDirectory.isDirectory()) {
            System.err.println(outputDirectory.getAbsolutePath()
                    + " exists, but is not a directory.");
            System.exit(1);
        }
        outputDirectory.mkdirs();

        // Consider Tor version to determine whether or not a relay can get
        // the Stable flag? Not considering increases performance by 1/4.
        // Note that after changing to true, the temporary table must be
        // re-written using --create-evaluation-table!
        boolean considerTorVersionForStability = false;

        // Sampling factor for considering only a subset of all relays for
        // performance reasons. Possible values are 1 for no sampling, or
        // 2, 4, 8, 16 for considering 1/2, 1/4, 1/8, 1/16 of all relays.
        // Note that after changing to true, the temporary table must be
        // re-written using --create-evaluation-table!
        int samplingFactor = 8;

        // connect to database
        String connectionURL = "jdbc:postgresql:" + args[createTable + 0];
        Connection conn = DriverManager.getConnection(connectionURL,
                "postgres", args[createTable + 1]);
        ResultSet rs = null;
        Statement s = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                     ResultSet.CONCUR_READ_ONLY);

        // prepare limits for later evaluations
        int configurations = 25;
        long[] bandwidthToGuaranteeFast = new long[configurations];
        long[] bandwidthToGuaranteeGuard = new long[configurations];
        long[] routerRequiredMinBandwidth = new long[configurations];
        long[] mtbfToGuaranteeStable = new long[configurations];
        long[] timeKnownToGuaranteeFamiliar = new long[configurations];
        double[] wfuToGuaranteeGuard = new double[configurations];
        double[] stableMtbfPercentile = new double[configurations];
        double[] fastBandwidthPercentile = new double[configurations];
        double[] guardBandwidthPercentile = new double[configurations];
        double[] timeKnownToGuaranteeFamiliarPercentile
                = new double[configurations];
        double[] wfuToGuaranteeGuardPercentile = new double[configurations];
        boolean[] guardsRequireFast = new boolean[configurations];
        boolean[] guardsRequireNonExit = new boolean[configurations];
        // set defaults
        for (int i = 0; i < configurations; i++) {
            bandwidthToGuaranteeFast[i] = 100L * 1024L;
            bandwidthToGuaranteeGuard[i] = 250L * 1024L;
            routerRequiredMinBandwidth[i] = 10000L;
            mtbfToGuaranteeStable[i] = 5L * 24L * 60L * 60L * 1000L;
            timeKnownToGuaranteeFamiliar[i] = 8L * 24L * 60L * 60L * 1000L;
            wfuToGuaranteeGuard[i] = 0.995;
            stableMtbfPercentile[i] = 50.0;
            fastBandwidthPercentile[i] = 12.5;
            guardBandwidthPercentile[i] = 50.0;
            timeKnownToGuaranteeFamiliarPercentile[i] = 12.5;
            wfuToGuaranteeGuardPercentile[i] = 50.0;
            guardsRequireFast[i] = true;
            guardsRequireNonExit[i] = true;
        }
        // set alternative requirements
        fastBandwidthPercentile[1] = 25.0;
        bandwidthToGuaranteeFast[2] = 50L * 1024L;
        stableMtbfPercentile[3] = 25.0;
        stableMtbfPercentile[4] = 37.5;
        stableMtbfPercentile[5] = 62.5;
        stableMtbfPercentile[6] = 75.0;
        mtbfToGuaranteeStable[7] = 3L * 24L * 60L * 60L * 1000L;
        mtbfToGuaranteeStable[8] = 4L * 24L * 60L * 60L * 1000L;
        mtbfToGuaranteeStable[9] = 6L * 24L * 60L * 60L * 1000L;
        mtbfToGuaranteeStable[10] = 7L * 24L * 60L * 60L * 1000L;
        guardsRequireFast[11] = false;
        guardsRequireNonExit[12] = false;
        guardBandwidthPercentile[13] = 25.0;
        guardBandwidthPercentile[14] = 37.5;
        guardBandwidthPercentile[15] = 62.5;
        guardBandwidthPercentile[16] = 75.0;
        timeKnownToGuaranteeFamiliarPercentile[17] = 25.0;
        timeKnownToGuaranteeFamiliarPercentile[18] = 37.5;
        timeKnownToGuaranteeFamiliarPercentile[19] = 50.0;
        wfuToGuaranteeGuard[20] = 0.7;
        wfuToGuaranteeGuard[21] = 0.8;
        wfuToGuaranteeGuard[22] = 0.9;
        wfuToGuaranteeGuard[23] = 0.95;
        wfuToGuaranteeGuard[24] = 0.99;

        // Prepare output
        File file = new File(outputDirectory.getAbsolutePath()
                    + File.separatorChar + "flagrequirements.csv");
        BufferedWriter out = new BufferedWriter(new FileWriter(file, false));
        StringBuilder sb = new StringBuilder("date");
        for (int i = 0; i < configurations; i++) {
            sb.append(",fast" + i + ",stable" + i + ",guard" + i);
        }
        out.write(sb.toString() + "\n");
        File file2 = new File(outputDirectory.getAbsolutePath()
                    + File.separatorChar + "requirements.csv");
        BufferedWriter reqOut = new BufferedWriter(new FileWriter(file2, false));
        sb = new StringBuilder("date");
        for (int i = 0; i < configurations; i++) {
            sb.append(",stablemtbf" + i + ",guardadvbw" + i);
        }
        reqOut.write(sb.toString() + "\n");

        // prepare evaluation table if required
        boolean evalTableExists = false;
        ResultSet resultSet = conn.getMetaData().getTables("%", "%", "%",
                new String[] { "TABLE" });
        while (resultSet.next()) {
            String tableName = resultSet.getString("TABLE_NAME");
            if (tableName.equalsIgnoreCase("flagseval")) {
                evalTableExists = true;
            }
        }
        if (createTable > 0 || !evalTableExists) {
            System.out.println("Preparing evaluation table...");
            long started = System.currentTimeMillis();
            if (evalTableExists) {
                s.execute("DROP TABLE flagseval");
            }
            s.execute("CREATE TABLE flagseval AS "
                    + "SELECT validafter, exit, fingerprint, published, "
                    + "uptime, LEAST(bandwidthavg, bandwidthobserved) "
                    + "AS bandwidthadvertised"
                    + (considerTorVersionForStability ? ", platform " : " ")
                    + "FROM statusentry NATURAL JOIN descriptor "
                    + "WHERE running = '1'"
                    + (samplingFactor == 1 ? ""
                        : "AND ASCII(fingerprint) < "
                        + (48 + 16 / samplingFactor)));
            System.out.println("Finished preparing evaluation table after "
                    + ((System.currentTimeMillis() - started) / 1000)
                    + " seconds.");
        }

        System.out.println("Querying from evaluation table...");
        long started = System.currentTimeMillis();
        rs = s.executeQuery("SELECT validafter, exit, fingerprint, "
                + "published, uptime, bandwidthadvertised"
                + (considerTorVersionForStability ? ", platform " : " ")            
                + "FROM flagseval ORDER BY validafter");
        System.out.println("Query executed after " + ((System.currentTimeMillis()
                - started) / 1000) + " seconds.");

        System.out.println("Starting evaluation... ");
        started = System.currentTimeMillis();
        Map<String, RelayHistory> history = new HashMap<String, RelayHistory>();
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

        // validafter times of current and last consensus
        long currentConsensusTime = -1, lastConsensusTime = -1;

        // Relays in current and last consensus
        Map<String, RelayHistory> currentRelays = null, lastRelays = null;

        // Number of flags and consensuses seen on 1 day
        int[] fast = new int[configurations];
        int[] stable = new int[configurations];
        int[] guard = new int[configurations];
        double[] stableMinimumMtbf = new double[configurations];
        long[] guardMinimumAdvBw = new long[configurations];
        int consensuses = 0;

        while (rs.next()) {
            long validAfter = rs.getTimestamp("validafter", cal).getTime();
            // is this the first or second consensus that we see?
            if (currentConsensusTime == -1 || lastConsensusTime == -1) {
                lastConsensusTime = currentConsensusTime;
                currentConsensusTime = validAfter;
                lastRelays = currentRelays;
                currentRelays = new HashMap<String, RelayHistory>();
            }
            // is this result set entry the first of a new consensus?
            if (validAfter > currentConsensusTime) {

                // (Finish the current consensus first before moving to the
                // consensus of this result set entry.)

                // Determine all non-running relays and notice that we know
                // all relays a little bit longer now (WFU).
                Set<RelayHistory> nonRunningRelays = new HashSet<RelayHistory>();
                for (Map.Entry<String, RelayHistory> relay : history.entrySet()) {
                    relay.getValue().totalTimeKnown += currentConsensusTime
                            - lastConsensusTime;
                    if (!currentRelays.containsKey(relay.getKey())) {
                        nonRunningRelays.add(relay.getValue());
                    }
                }

                // MTBF: Consider all relays we have *not* seen in this consensus as down.
                for (RelayHistory hist : nonRunningRelays) {

                    // Was relay up before this consensus? If so, end uptime session.
                    if (hist.currentSessionStart > 0) {
                        hist.uptimeLength += (double) (lastConsensusTime
                                - hist.currentSessionStart);
                        hist.uptimeNumbers += 1.0;
                        hist.currentSessionStart = 0;
                    }
                }

                // Calculate limits and number of flags for all configurations
                for (int i = 0; i < configurations; i++) {

                    // Iterate over all running relays and collect data about
                    // them to determine current limits
                    List<Double> mtbfs = new ArrayList<Double>();
                    List<Double> timeKnowns = new ArrayList<Double>();
                    List<Long> bandwidths = new ArrayList<Long>();
                    List<Long> bandwidthsExcludingExits = new ArrayList<Long>();
                    for (RelayHistory hist : currentRelays.values()) {
                        bandwidths.add(hist.advertisedBandwidth);
                        if (!guardsRequireNonExit[i] || !hist.exit) {
                            bandwidthsExcludingExits.add(hist.advertisedBandwidth);
                        }
                        timeKnowns.add((double) hist.totalTimeKnown);
                        // Consider current uptime session for MTBF, too.
                        hist.mtbf = (hist.uptimeLength + currentConsensusTime
                                - hist.currentSessionStart)
                                / (hist.uptimeNumbers + 1.0);
                        mtbfs.add(hist.mtbf);
                    }

                    // Determine MTBF for Stable flag
                    double stableMtbf = percentile(mtbfs,
                            stableMtbfPercentile[i]);

                    // Determine bandwidth limits
                    long fastBandwidth = percentile(bandwidths,
                            fastBandwidthPercentile[i]);
                    if (fastBandwidth < routerRequiredMinBandwidth[i]) {
                        fastBandwidth = percentile(bandwidths,
                                fastBandwidthPercentile[i] * 2 < 100.0
                                ? fastBandwidthPercentile[i] * 2 : 100.0);
                    }
                    if (fastBandwidth > bandwidthToGuaranteeFast[i]) {
                        fastBandwidth = bandwidthToGuaranteeFast[i];
                    }
                    long guardBandwidthExcludingExits
                            = percentile(bandwidthsExcludingExits,
                            guardBandwidthPercentile[i]);

                    // Determine WFU of familiar relays
                    double guardTimeKnown = percentile(timeKnowns,
                            timeKnownToGuaranteeFamiliar[i]);
                    if (guardTimeKnown > timeKnownToGuaranteeFamiliar[i]) {
                        guardTimeKnown = timeKnownToGuaranteeFamiliar[i];
                    }
                    List<Double> wfus = new ArrayList<Double>();
                    for (RelayHistory hist : currentRelays.values()) {
                        if (hist.totalTimeKnown >= guardTimeKnown) {
                            wfus.add(hist.totalUptime / hist.totalTimeKnown);
                        }
                    }
                    double guardWfu = percentile(wfus,
                             wfuToGuaranteeGuardPercentile[i]);
                    if (guardWfu > wfuToGuaranteeGuard[i]) {
                        guardWfu = wfuToGuaranteeGuard[i];
                    }

                    // Count flags in current consensus
                    for (RelayHistory hist : currentRelays.values()) {
                        if ((!considerTorVersionForStability
                               || !hist.unstableVersion)
                               && (hist.mtbf >= stableMtbf
                               || hist.mtbf >= mtbfToGuaranteeStable[i])) {
                            stable[i]++;
                        }
                        if (hist.advertisedBandwidth >= fastBandwidth) {
                            fast[i]++;
                        }
                        if ((!guardsRequireFast[i]
                                || hist.advertisedBandwidth >= fastBandwidth)
                                && (!guardsRequireNonExit[i] || !hist.exit)
                                && (hist.advertisedBandwidth >= bandwidthToGuaranteeGuard[i]
                                || hist.advertisedBandwidth >= guardBandwidthExcludingExits)
                                && hist.totalUptime / hist.totalTimeKnown >= guardWfu
                                && hist.totalTimeKnown >= guardTimeKnown) {
                            guard[i]++;
                        }
                    }

                    // Write minimum MTBF, WFU, etc. to disk
                    stableMinimumMtbf[i] += stableMtbf;
                    guardMinimumAdvBw[i] += Math.min(bandwidthToGuaranteeGuard[i],
                            guardBandwidthExcludingExits);
                }
                consensuses++;

                // Have 12 hours (or even more) passed? If so, discount old data.
                while (validAfter / TWELVE_HOURS
                        > currentConsensusTime / TWELVE_HOURS) {

                    for (RelayHistory hist : history.values()) {
                        hist.totalUptime *= 0.95;
                        hist.totalTimeKnown *= 0.95;
                        hist.uptimeLength *= 0.95;
                        hist.uptimeNumbers *= 0.95;
                    }

                    // Have 24 hours passed? If so, write results for this day to disk.
                    if (validAfter / TWENTY_FOUR_HOURS
                            > currentConsensusTime / TWENTY_FOUR_HOURS) {
                        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
                        StringBuilder currentOut = new StringBuilder(
                                formatter.format(new Date(TWELVE_HOURS
                                * (currentConsensusTime / TWELVE_HOURS + 1))));
                        StringBuilder requirementsOut = new StringBuilder(
                                formatter.format(new Date(TWELVE_HOURS
                                * (currentConsensusTime / TWELVE_HOURS + 1))));
                        for (int i = 0; i < configurations; i++) {
                            if (consensuses > 0) {
                                currentOut.append(","
                                    + (samplingFactor * fast[i] / consensuses) + ","
                                    + (samplingFactor * stable[i] / consensuses) + ","
                                    + (samplingFactor * guard[i] / consensuses));
                                requirementsOut.append(","
                                    + (long) ((double) samplingFactor * stableMinimumMtbf[i] / (double) consensuses) + ","
                                    + (samplingFactor * guardMinimumAdvBw[i] / consensuses));
                            } else {
                                currentOut.append(",NA,NA,NA");
                                requirementsOut.append(",NA,NA,NA");
                            }
                            fast[i] = 0;
                            stable[i] = 0;
                            guard[i] = 0;
                        }
                        out.write(currentOut.toString() + "\n");
                        System.out.println(currentOut.toString());
                        reqOut.write(requirementsOut.toString() + "\n");
                        System.out.println(requirementsOut.toString());
                        consensuses = 0;
                    }

                    // Move currentConsensusTime to validafter or to next
                    // 12-hour interval in case we are missing some consensuses
                    currentConsensusTime = validAfter <= TWELVE_HOURS
                            * (currentConsensusTime / TWELVE_HOURS + 1)
                            ? validAfter
                            : TWELVE_HOURS * (currentConsensusTime / TWELVE_HOURS + 1);
                }

                // Move on to next consensus
                lastConsensusTime = currentConsensusTime;
                currentConsensusTime = validAfter;
                lastRelays = currentRelays;
                currentRelays = new HashMap<String, RelayHistory>();

                // Clean up some memory.
                System.gc();
            }

            // Read rest of this result set entry
            String fingerprint = rs.getString("fingerprint");
            long published = rs.getTimestamp("published", cal).getTime();
            long uptime = rs.getLong("uptime");
            long advertisedBandwidth = rs.getLong("bandwidthadvertised");
            int exit = rs.getInt("exit");
            RelayHistory hist = null;

            // Did we see this relay before?
            if (history.containsKey(fingerprint)) {
                hist = history.get(fingerprint);

                // Did we see this relay in the last consensus?
                if (lastRelays.containsKey(fingerprint)) {
                    // WFU: Add whole time difference between this consensus
                    // and the last as uptime.
                    hist.totalUptime += validAfter - lastConsensusTime;

                    // MTBF: Was relay restarted since we saw it last time?
                    if (uptime < hist.lastUptime) {
                        // Add session length from start to this restart to
                        // total session length and start new session
                        hist.uptimeLength += published - uptime - hist.currentSessionStart;
                        hist.uptimeNumbers += 1.0;
                        hist.currentSessionStart = published - uptime;
                    }
                } else {
                    // WFU: Relay was started after last consensus. Only
                    // consider time since start.
                    hist.totalUptime += validAfter - published + uptime;

                    // MTBF: Start new session
                    hist.currentSessionStart = published - uptime;
                }
            } else {
                // Create and add new history object for this relay
                hist = new RelayHistory();
                history.put(fingerprint, hist);

                // Initialize total uptime for WFU
                hist.totalTimeKnown = hist.totalUptime = validAfter - published + uptime;

                // Remember uptime sessions start for MTBF
                hist.currentSessionStart = published - uptime;
            }

            // Prepare calculation of limits for this consensus
            hist.advertisedBandwidth = advertisedBandwidth;
            if (considerTorVersionForStability) {
                hist.unstableVersion = true;
                String platform = rs.getString("platform");
                if (platform.length() > 4
                        && platform.split(" ").length > 1
                        && platform.split(" ")[1].split("[^0-9]").length > 3) {
                    String[] ver = platform.split(" ")[1].split("[^0-9]");
                    if (Integer.parseInt(ver[1]) > 1
                            || (Integer.parseInt(ver[1]) >= 1
                            && Integer.parseInt(ver[2]) > 1
                            || (Integer.parseInt(ver[2]) >= 1
                            && Integer.parseInt(ver[3]) >= 16))) {
                        hist.unstableVersion = false;
                    }
                }
            }
            hist.exit = (exit == 1);
            hist.lastUptime = uptime;
            currentRelays.put(fingerprint, hist);
        }

        // We are done. Disconnect from database and exit.
        out.close();
        reqOut.close();
        conn.close();
        System.out.println("Evaluation finished after " + ((System.currentTimeMillis()
                - started) / 1000) + " seconds.");
    }
}

