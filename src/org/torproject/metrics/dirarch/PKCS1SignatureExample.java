package org.torproject.metrics.dirarch;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.Signature;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 * Generating a PKCS1 v1.5 style signature.
 */
public class PKCS1SignatureExample {
	public static void main(String[] args) throws Exception {
		java.security.Security.addProvider(new BouncyCastleProvider());
		KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA", "BC");
		keyGen.initialize(1024, new SecureRandom());
		KeyPair keyPair = keyGen.generateKeyPair();
		Signature signature = Signature.getInstance("SHA1withRSA", "BC");

		// generate a signature
		signature.initSign(keyPair.getPrivate());
		byte[] message = "hallo".getBytes();
		signature.update(message);
		byte[] sigBytes = signature.sign();

		// verify a signature
		signature.initVerify(keyPair.getPublic());
		signature.update(message);
		if (signature.verify(sigBytes)) {
			System.out.println("signature verification succeeded.");
		} else {
			System.out.println("signature verification failed.");
		}
	}
}