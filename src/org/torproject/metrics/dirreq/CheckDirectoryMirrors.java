/* Copyright 2009 Karsten Loesing
 * See LICENSE for licensing information */
package org.torproject.metrics.dirreq;

import java.io.*;
import java.text.*;
import java.util.*;
import java.net.*;

public final class CheckDirectoryMirrors {

    private static class Mirror {
        String nickname;
        String identity;
        String ipAddress;
        int dirPort;
        int bandwidth;
        boolean works = false;
        int statusCode = -1;
        long started, finished;
        synchronized void setWorks() {
            this.finished = System.currentTimeMillis();
            this.works = true;
        }
        void startRequest() {
            this.started = System.currentTimeMillis();
        }
        synchronized boolean works() {
            return works;
        }
        synchronized void setStatusCode(int statusCode) {
            this.statusCode = statusCode;
        }
        synchronized int getStatusCode() {
            return this.statusCode;
        }
        synchronized long getDownloadTime() {
            if (!this.works) {
                return -1;
            } else {
                return (this.finished - this.started);
            }
        }
    }

    private CheckDirectoryMirrors() {
    }

    private static class RequestThread extends Thread {
        boolean parse;
        boolean waiting = true;
        boolean successful = false;
        Mirror mirror;
        List<Mirror> mirrors;
        public RequestThread(Mirror mirror, boolean parse) {
            this.mirror = mirror;
            this.parse = parse;
            if (parse) {
                this.mirrors = new ArrayList<Mirror>();
            }
        }
        public void run() {
            this.mirror.startRequest();
            BufferedReader is = null;
            try {
                String fileAddress = "http://" + mirror.ipAddress + ":"
                        + mirror.dirPort + "/tor/status-vote/current/consensus";
                System.out.println("Downloading from: " + fileAddress);
                InetAddress ia = InetAddress.getByName(mirror.ipAddress);
                Socket s = new Socket(ia, mirror.dirPort);
                PrintStream out = new PrintStream(s.getOutputStream());
                out.print("GET /tor/status-vote/current/consensus HTTP/1.0\r\n\r\n");
                is = new BufferedReader(
                        new InputStreamReader(s.getInputStream()));
                int bytesRead, bytesWritten = 0;
                boolean downloading = false;
                boolean firstLine = true;
                String line = null;
                String currentDate = null;
                Mirror currentMirror = null;
                while ((line = is.readLine()) != null) {
                    if (!downloading) {
                        System.out.println("downloading...");
                        downloading = true;
                    }
                    if (firstLine) {
                        if (line.split(" ").length > 1) {
                            try {
                                int statusCode = Integer.parseInt(line.split(" ")[1]);
                                this.mirror.setStatusCode(statusCode);
                            } catch (NumberFormatException ex) {
                                System.out.printf("Problem parsing status code from '%s'%n",
                                        line);
                            }
                        }
                        System.out.println("First line: " + line);
                        firstLine = false;
                    }
                    if (this.parse) {
                        if (line.startsWith("r ")) {
                            String[] parts = line.split(" ");
                            int dirPort = Integer.parseInt(parts[8]);
                            if (dirPort > 0) {
                                currentMirror = new Mirror();
                                currentMirror.nickname = parts[1];
                                currentMirror.identity = parts[2];
                                currentMirror.ipAddress = parts[6];
                                currentMirror.dirPort = dirPort;
                                mirrors.add(currentMirror);
                            } else {
                                currentMirror = null;
                            }
                        } else if (currentMirror != null && line.startsWith("w ")) {
                            try {
                                currentMirror.bandwidth = Integer.parseInt(line.split(" ")[1].split("=")[1]);
                            } catch (Exception ex) {
                                System.out.printf("Could not parse advertised bandwidth from line '%s'.%n", line);
                            }
                        }
                    }
                }
                System.out.println("Downloaded successfully.");
                this.mirror.setWorks();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            this.successful = true;
        }
        public synchronized void stopWaiting() {
            this.waiting = false;
        }
        public synchronized boolean stillWaiting() {
            return this.waiting;
        }
        public boolean wasSuccessful() {
            return this.successful;
        }
        public List<Mirror> getMirrors() {
            return this.mirrors;
        }
    }

    public static void main(final String[] args) throws Exception {

        // check input parameters
        if (args.length < 1) {
            System.err.println("Usage: java "
                    + CheckDirectoryMirrors.class.getSimpleName()
                    + " <output directory>");
            System.exit(1);
        }
        File outputDirectory = new File(args[0]);
        if (outputDirectory.exists() && !outputDirectory.isDirectory()) {
            System.err.println("Output directory '"
                    + outputDirectory.getAbsolutePath()
                    + "' exists, but is not a directory.");
            System.exit(1);
        }
        outputDirectory.mkdir();

        // download consensus from gabelmoo
        System.out.println("Starting requests at " + System.currentTimeMillis());
        Mirror gabelmoo = new Mirror();
        gabelmoo.ipAddress = "80.190.246.100";
        gabelmoo.dirPort = 80;
        RequestThread download = new RequestThread(gabelmoo, true);
        download.start();
        try {
            Thread.sleep(60L * 1000L);
        } catch (InterruptedException ex) {
            System.out.println("Interrupted while waiting for consensus download.");
        }
        download.stopWaiting();
        if (!download.wasSuccessful()) {
            System.out.println("Could not download consensus from gabelmoo. Exiting!");
            System.exit(1);
        }
        System.out.println("Got the current consensus. Trying all directory mirrors..");
        List<Mirror> mirrors = download.getMirrors();
        Collections.shuffle(mirrors);
        for (Mirror mirror : mirrors) {
            long startedRequest = System.currentTimeMillis();
            RequestThread thread = new RequestThread(mirror, false);
            thread.start();
            // start requests with 5 seconds delay
            try {
                Thread.sleep(15L * 1000L);
            } catch (InterruptedException ex) {
                System.out.println("Interrupted while waiting for request thread.");
            }
        }

        // wait for another ten minutes to let them finish
        try {
            Thread.sleep(10L * 60L * 1000L);
        } catch (InterruptedException ex) {
            System.out.println("Interrupted while waiting final two minutes.");
        }

        File outFile = new File(outputDirectory.getAbsolutePath()
                + File.separatorChar + "mirrors.csv");
        BufferedWriter out = new BufferedWriter(new FileWriter(
                outFile, false));
        out.write("identity,nickname,bandwidth,statuscode,downloadtime\n");
        for (Mirror mirror : mirrors) {
            out.write(mirror.identity + "," + mirror.nickname + ","
                    + mirror.bandwidth + "," + mirror.getStatusCode() + ","
                    + mirror.getDownloadTime() + "\n");
        }
        out.close();
    }
}

