/* Copyright 2008-2009 Karsten Loesing
 * See LICENSE for licensing information */
package org.torproject.metrics.util;

import org.bouncycastle.crypto.digests.SHA1Digest;

/**
 * Provides crypto operations.
 */
public final class Crypto {

    private Crypto() {
    }

    /**
     * Determine SHA1 hash of input.
     */
    public static byte[] getHash(final byte[] input) {
        SHA1Digest sha1 = new SHA1Digest();
        sha1.reset();
        sha1.update(input, 0, input.length);
        byte[] hash = new byte[sha1.getDigestSize()];
        sha1.doFinal(hash, 0);
        return hash;
    }
}

