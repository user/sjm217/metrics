/* Copyright 2008-2009 Karsten Loesing
 * See LICENSE for licensing information */
package org.torproject.metrics.bridges;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.TreeSet;
import org.torproject.metrics.util.Crypto;
import org.torproject.metrics.util.Encoding;

/**
 * Parse archived bridge data.
 *
 * @author karsten
 */
public final class ParseBridgeData {

    private ParseBridgeData() {
    }

    private static class Networkstatus {
        Set<Router> entries;
    }

    private static class Router {
        Descriptor descriptor;
        // some of the following fields are not used by the analysis, but
        // are left here as placeholders.
        // String nickname;
        // String identity;
        String descriptorDigest;
        // String published;
        // String ipAddress;
        // String orPort;
        // boolean fast;
        // boolean guard;
        // boolean hsdir;
        boolean running;
        // boolean stable;
        // boolean v2dir;
        // boolean valid;
    }

    private static class Descriptor {
        Extrainfo extrainfo;
        // String nickname;
        // String ipAddress;
        // String orPort;
        // String platform;
        // String published;
        // String fingerprint;
        // long uptime;
        // String averageBandwidth;
        // String maxBandwidth;
        // long observedBandwidth;
        String extrainfoDigest;
    }

    private static class Extrainfo {
        // String nickname;
        String fingerprint;
        String published;
        String writeHistory;
        String readHistory;
        String geoipStartTime;
        String geoipClientOrigins;
        long averageWrite;
        long averageRead;
        boolean possiblyRelay;
    }

    public static void main(final String[] args) throws Exception {
        if (args.length < 4) {
            System.err.println("Usage: java "
                    + ParseBridgeData.class.getSimpleName()
                    + " <path to data/ directory> <database name> "
                    + "<database password> <output directory>");
            System.exit(1);
        }
        File testenvDirectory = new File(args[0]);
        if (!testenvDirectory.exists() || !testenvDirectory.isDirectory()) {
            System.err.println(testenvDirectory.getAbsolutePath()
                    + " does not exist or is not a directory.");
            System.exit(1);
        }
        String dbName = args[1];
        String dbPass = args[2];
        File outputDirectory = new File(args[3]);
        if (outputDirectory.exists() && !outputDirectory.isDirectory()) {
            System.err.println(outputDirectory.getAbsolutePath()
                    + " exists, but is not a directory.");
            System.exit(1);
        }
        outputDirectory.mkdir();

        // create data structure to hold parsed data
        SortedMap<Long, Networkstatus> networkstatuses =
                new TreeMap<Long, Networkstatus>();

        // prepare output file(s)
        File runningOutFile = new File(outputDirectory.getAbsolutePath()
                + File.separatorChar + "running.csv");
        File bandwidthOutFile = new File(outputDirectory.getAbsolutePath()
                + File.separatorChar + "bandwidth.csv");
        File geoipOutFile = new File(outputDirectory.getAbsolutePath()
                + File.separatorChar + "geoip.csv");
        File geoipDailyOutFile = new File(outputDirectory.getAbsolutePath()
                + File.separatorChar + "geoip-daily.csv");
        BufferedWriter runningOut = new BufferedWriter(new FileWriter(
                runningOutFile, false));
        runningOut.write("time,total,running\n");
        BufferedWriter bandwidthOut = new BufferedWriter(new FileWriter(
                bandwidthOutFile, false));
        bandwidthOut.write("time,bandwidth,norouter,noextra,nohistory,"
                + "nogeoip,running\n");

        BufferedWriter geoipOut = new BufferedWriter(new FileWriter(
                geoipOutFile, false));
        BufferedWriter geoipDailyOut = new BufferedWriter(new FileWriter(
                geoipDailyOutFile, false));
        SortedSet<String> allCountryCodes = new TreeSet<String>();
        String[] allCC = new String[] { "ad", "ae", "af", "ag", "ai", "al",
                "am", "an", "ao", "aq", "ar", "as", "at", "au", "aw", "ax",
                "az", "ba", "bb", "bd", "be", "bf", "bg", "bh", "bi", "bj",
                "bm", "bn", "bo", "br", "bs", "bt", "bv", "bw", "by", "bz",
                "ca", "cd", "cf", "cg", "ch", "ci", "ck", "cl", "cm", "cn",
                "co", "cr", "cs", "cu", "cv", "cy", "cz", "de", "dj", "dk",
                "dm", "do", "dz", "ec", "ee", "eg", "er", "es", "et", "fi",
                "fj", "fk", "fm", "fo", "fr", "ga", "gb", "gd", "ge", "gf",
                "gh", "gi", "gl", "gm", "gn", "gp", "gq", "gr", "gs", "gt",
                "gu", "gw", "gy", "hk", "hn", "hr", "ht", "hu", "id", "ie",
                "il", "im", "in", "io", "iq", "ir", "is", "it", "je", "jm",
                "jo", "jp", "ke", "kg", "kh", "ki", "km", "kn", "kr", "kw",
                "ky", "kz", "la", "lb", "lc", "li", "lk", "lr", "ls", "lt",
                "lu", "lv", "ly", "ma", "mc", "md", "me", "mg", "mh", "mk",
                "ml", "mm", "mn", "mo", "mp", "mq", "mr", "ms", "mt", "mu",
                "mv", "mw", "mx", "my", "mz", "na", "nc", "ne", "nf", "ng",
                "ni", "nl", "no", "np", "nr", "nu", "nz", "om", "pa", "pe",
                "pf", "pg", "ph", "pk", "pl", "pr", "ps", "pt", "pw", "py",
                "qa", "re", "ro", "rs", "ru", "rw", "sa", "sb", "sc", "sd",
                "se", "sg", "si", "sk", "sl", "sm", "sn", "so", "sr", "st",
                "sv", "sy", "sz", "tc", "td", "tf", "tg", "th", "tj", "tk",
                "tl", "tm", "tn", "to", "tr", "tt", "tv", "tw", "ua", "ug",
                "um", "us", "uy", "uz", "va", "vc", "ve", "vg", "vi", "vn",
                "vu", "wf", "ws", "ye", "yt", "za", "zm", "zw" };
        geoipOut.write("time,");
        geoipDailyOut.write("time,");
        for (String c : allCC) {
            geoipOut.write(c + ",");
            geoipDailyOut.write(c + ",");
            allCountryCodes.add(c);
        }
        geoipOut.write("total\n");
        geoipDailyOut.write("total\n");

        // obtain list of all bridge fingerprints
        long started = System.currentTimeMillis();
        File[] snapshots = testenvDirectory.listFiles();
        Set<String> allBridges = new HashSet<String>();
        for (File subdirectory : snapshots) {
            if (!subdirectory.isDirectory()) {
                System.err.println(subdirectory.getName()
                        + " is not a directory.");
                System.exit(1);
            }
            File networkstatusFile = new File(subdirectory.getAbsolutePath()
                    + File.separatorChar + "networkstatus-bridges");
            if (!networkstatusFile.exists()) {
                System.err.println("Missing networkstatus-bridges file in "
                        + subdirectory.getAbsolutePath());
                System.exit(1);
            }
            BufferedReader br = new BufferedReader(new FileReader(
                    networkstatusFile));
            String line = null;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("r ")) {
                    allBridges.add(Encoding.toHex(Encoding.fromBase64(
                            line.split(" ")[2] + "=")));
                }
            }
            br.close();
        }
        System.out.println("We have seen " + allBridges.size() + " bridges. "
                + ((System.currentTimeMillis() - started) / 1000)
                + " seconds");

        // add list of bridge fingerprints to database
        started = System.currentTimeMillis();
        String connectionURL = "jdbc:postgresql:" + dbName;
        Connection conn = DriverManager.getConnection(connectionURL,
                "postgres", dbPass);
        ResultSet resultSet = conn.getMetaData().getTables("%", "%", "%",
                new String[] { "TABLE" });
        Statement statement = conn.createStatement();
        while (resultSet.next()) {
            String tableName = resultSet.getString("TABLE_NAME");
            if (tableName.equalsIgnoreCase("bridges")) {
                statement.execute("DROP TABLE bridges");
                break;
            }
        }
        statement.execute("CREATE TABLE bridges ("
                + "fingerprint CHAR(40) NOT NULL PRIMARY KEY)");
        PreparedStatement psB = conn.prepareStatement("INSERT INTO bridges "
                + "(fingerprint) VALUES (?)");
        for (String s : allBridges) {
            psB.setString(1, s);
            psB.addBatch();
        }
        psB.executeBatch();
        System.out.println("Added bridges to descriptor database. "
                + ((System.currentTimeMillis() - started) / 1000)
                + " seconds");

        // request list of all relays that have been seen as bridges, including
        // router descriptor publication times
        started = System.currentTimeMillis();
        resultSet = statement.executeQuery("SELECT fingerprint, published "
                + "FROM descriptor NATURAL JOIN bridges");
        Map<String, SortedSet<Long>> bridgeRelayPublications =
                new HashMap<String, SortedSet<Long>>();
        Calendar calend = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        while (resultSet.next()) {
            String fingerprint = resultSet.getString("fingerprint");
            long timestamp = resultSet.getTimestamp("published", calend).getTime();
            if (!bridgeRelayPublications.containsKey(fingerprint)) {
                bridgeRelayPublications.put(fingerprint, new TreeSet<Long>());
            }
            bridgeRelayPublications.get(fingerprint).add(timestamp);
        }
        System.out.println("Identified " + bridgeRelayPublications.size()
                + " relays that have also been seen as bridges. "
                + ((System.currentTimeMillis() - started) / 1000)
                + " seconds");

        // parse log files
        started = System.currentTimeMillis();
        List<File> sortedDirectories = new ArrayList<File>(snapshots.length);
        for (File s : snapshots) {
            sortedDirectories.add(s);
        }
        Collections.sort(sortedDirectories);
        int numSnapshots = sortedDirectories.size();
        int parseProgress = 0;
        int allTotals = 0, allRunnings = 0, timestampsSeen = 0;
        long allBandwidths = 0L, date = -1L;
        int month = -1;
        int noRouterDesc = 0, noExtraInfo = 0, noHistory = 0, noGeoip = 0;
        Map<String, Double> geoipClients = new HashMap<String, Double>();
        Map<String, Double> geoipDailyClients = new HashMap<String, Double>();
        SimpleDateFormat timeFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        for (File subdirectory : sortedDirectories) {

            // check if files are where we expect them
            if (!subdirectory.isDirectory()) {
                System.err.println(subdirectory.getName()
                        + " is not a directory.");
                System.exit(1);
            }
            File networkstatusFile = new File(subdirectory.getAbsolutePath()
                    + File.separatorChar + "networkstatus-bridges");
            File bridgeDescriptorsFile = new File(subdirectory
                    .getAbsolutePath()
                    + File.separatorChar + "bridge-descriptors");
            File cachedExtrainfoFile = new File(subdirectory.getAbsolutePath()
                    + File.separatorChar + "cached-extrainfo");
            File cachedExtrainfoFileNew = new File(subdirectory
                    .getAbsolutePath()
                    + File.separatorChar + "cached-extrainfo.new");
            if (!networkstatusFile.exists()) {
                System.err.println("Missing networkstatus-bridges file in "
                        + subdirectory.getAbsolutePath());
                System.exit(1);
            }

            // extract a useful timestamp (from strings like 2008-11-19T023703Z)
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HHmm");
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            long millisSince1970 = format.parse(
                    subdirectory.getName().substring(0, 15).replace('T', ' '),
                    new ParsePosition(0)).getTime();
            long timestamp = millisSince1970 / 1800000 * 1800000;
            Networkstatus networkstatus = new Networkstatus();
            networkstatus.entries = new HashSet<Router>();
            networkstatuses.put(timestamp, networkstatus);

            // read network status
            BufferedReader br = new BufferedReader(new FileReader(
                    networkstatusFile));
            String line = null;
            String rLine = null;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("r ")) {
                    rLine = line;
                } else if (line.startsWith("s ")) {
                    String[] splitted = rLine.split(" ");
                    Router entry = new Router();
                    // unused fields are not written to data structure
                    // entry.nickname = splitted[1];
                    // entry.identity = splitted[2];
                    entry.descriptorDigest = splitted[3];
                    // entry.published = splitted[4] + " " + splitted[5];
                    // entry.ipAddress = splitted[6];
                    // entry.orPort = splitted[7];
                    // entry.fast = line.contains("Fast");
                    // entry.guard = line.contains("Guard");
                    // entry.hsdir = line.contains("HSDir");
                    entry.running = line.contains("Running");
                    // entry.stable = line.contains("Stable");
                    // entry.v2dir = line.contains("V2Dir");
                    // entry.valid = line.contains("Valid");
                    networkstatus.entries.add(entry);
                }
            }
            br.close();

            // read descriptors
            if (bridgeDescriptorsFile.exists()) {
                Map<String, Descriptor> descriptors =
                        new HashMap<String, Descriptor>();
                br = new BufferedReader(new FileReader(bridgeDescriptorsFile));
                StringBuilder digestBuilder = new StringBuilder();
                Descriptor d = null;
                while ((line = br.readLine()) != null) {
                    if (line.startsWith("@")) {
                        continue;
                    } else if (line.startsWith("router ")) {
                        digestBuilder = new StringBuilder();
                        d = new Descriptor();
                        String[] parts = line.split(" ");
                        // unused fields
                        // d.nickname = parts[1];
                        // d.ipAddress = parts[2];
                        // d.orPort = parts[3];
                        // } else if (line.startsWith("platform ")) {
                        // d.platform = line.substring(9);
                        // } else if (line.startsWith("published ")) {
                        // d.published = line.substring(10);
                        // } else if (line.startsWith("opt fingerprint ")) {
                        // d.fingerprint = line.substring(16);
                        // } else if (line.startsWith("uptime ")) {
                        // d.uptime = Long.parseLong(line.substring(7));
                        // } else if (line.startsWith("bandwidth ")) {
                        // String[] parts = line.split(" ");
                        // d.averageBandwidth = parts[1];
                        // d.maxBandwidth = parts[2];
                        // d.observedBandwidth = Long.parseLong(parts[3]);
                    } else if (line.startsWith("opt extra-info-digest ")) {
                        d.extrainfoDigest = line.substring(22);
                    } else if (line.startsWith("router-signature")) {
                        // calculate hash
                        String desc = digestBuilder.toString() + line + "\n";
                        byte[] hash = Crypto.getHash(desc.getBytes());
                        String out = Encoding
                                .toBase64NoTrailingEqualSigns(hash);
                        descriptors.put(out, d);
                    }
                    digestBuilder.append(line + "\n");
                }
                br.close();
                for (Router entry : networkstatus.entries) {
                    if (descriptors.containsKey(entry.descriptorDigest)) {
                        entry.descriptor = descriptors
                                .get(entry.descriptorDigest);
                    }
                }

                // read extrainfos
                if (cachedExtrainfoFile.exists()) {
                    Map<String, Extrainfo> extrainfos =
                            new HashMap<String, Extrainfo>();
                    br = new BufferedReader(
                            new FileReader(cachedExtrainfoFile));
                    digestBuilder = null;
                    Extrainfo e = null;
                    int filesToParse = 2; // cached-extrainfo and
                    // cached-extrainfo.new
                    while (filesToParse > 0) {
                        line = br.readLine();
                        if (line == null) {
                            if (--filesToParse == 1) {
                                // at the end of cached-extrainfos file, read
                                // .new file now
                                br.close();
                                if (!cachedExtrainfoFileNew.exists()) {
                                    --filesToParse;
                                } else {
                                    br = new BufferedReader(new FileReader(
                                            cachedExtrainfoFileNew));
                                }
                            }
                            continue;
                        }
                        if (line.startsWith("@")) {
                            continue;
                        } else if (line.startsWith("extra-info ")) {
                            digestBuilder = new StringBuilder();
                            e = new Extrainfo();
                            String[] parts = line.split(" ");
                            // e.nickname = parts[1];
                            e.fingerprint = parts[2];
                        } else if (line.startsWith("write-history ")) {
                            e.writeHistory = line.substring(14);
                        } else if (line.startsWith("read-history ")) {
                            e.readHistory = line.substring(13);
                        } else if (line.startsWith("published ")) {
                            e.published = line.substring(10);
                        } else if (line.startsWith("geoip-start-time ")) {
                            e.geoipStartTime = line.substring(17);
                        } else if (line.startsWith("geoip-client-origins ")) {
                            e.geoipClientOrigins = line.substring(21);
                        } else if (line.startsWith("router-signature")) {
                            // calculate hash
                            String desc = digestBuilder.toString() + line
                                    + "\n";
                            byte[] hash = Crypto.getHash(desc.getBytes());
                            String out = Encoding.toHex(hash);
                            // did this bridge run as relay recently?
                            // a) don't care, evaluate all bridges -> never set
                            //    e.possiblyRelay to true
                            // b) exclude only those bridges that were seen as
                            //    relays 7 days or less before publishing their
                            //    bridge extra-info document.
/* The debug statements below consider 4 possible cases with respect to
   bridges that have been seen as relays:
1. The bridge has published the same descriptor as relay, too. Excluding.
2. The bridge has published a descriptor as relay 7 days before publishing
   the considered bridge descriptor. Excluding.
3. The bridge has published a descriptor as relay more than 7 days before
   publishing the considered bridge descriptor. Not excluding.
4. The bridge has published a descriptor as relay after publishing the
   considered bridge descriptor. Not excluding. */
                            // c) exclude all bridges that were seen as relay
                            //    at any time.
                            if (bridgeRelayPublications.containsKey(
                                    e.fingerprint)) {
                                // calculate bandwidth that this relay has seen at the publication day.
                                long totalBandwidth = 0;
                                if (e.writeHistory != null && e.readHistory != null) {
                                    String[] write1 = e.writeHistory.split(" ");
                                    String[] read1 = e.readHistory.split(" ");
                                    if (write1.length > 4 && read1.length > 4) {
                                        String[] write2 = write1[4].split(",");
                                        String[] read2 = read1[4].split(",");
                                        if (write2.length == read2.length) {
                                            long num = 0, sum = 0;
                                            for (int i = 0; i < write2.length;
                                                    i++) {
                                                num++;
                                                sum += (Long.parseLong(write2[i])
                                                        + Long
                                                        .parseLong(read2[i])) / 2;
                                            }
                                            totalBandwidth = (sum / num);
                                        }
                                    }
                                }
                                // (begin of variant b)
                                long extraPubl = timeFormat.parse(e.published)
                                        .getTime();
                                long extraPublDay = extraPubl / 86400000;
                                if (bridgeRelayPublications.get(e.fingerprint)
                                        .contains(extraPubl)) {
/* System.out.println("Case1: Bridge " + e.fingerprint + " has published "
+ "an extra info descriptor at " + e.published + " as relay at the same time "
+ "as publishing one as bridge, having a bandwidth of " + totalBandwidth
+ ". Excluding."); */
                                    e.possiblyRelay = true;
                                } else if (!bridgeRelayPublications.get(
                                        e.fingerprint).headSet(extraPubl)
                                        .isEmpty()) {
                                    long lastRelayPublBefore
                                            = bridgeRelayPublications.get(
                                            e.fingerprint).headSet(
                                            extraPubl).last();
                                    if (extraPubl - lastRelayPublBefore
                                            < 7L * 24L * 60L * 60L * 1000L) {
/* System.out.println("Case2: Bridge " + e.fingerprint + " has published an "
+ "extra info " + "descriptor at " + e.published + " as relay "
+ ((extraPubl - lastRelayPublBefore) / (24L * 60L * 60L * 1000L)) + " days = "
+ ((extraPubl - lastRelayPublBefore) / (60L * 1000L)) + " minutes = "
+ (extraPubl - lastRelayPublBefore) + " milliseconds before publishing one as "
+ "bridge, having a bandwidth of " + totalBandwidth + ". Excluding."); */
                                        e.possiblyRelay = true;
                                    } else {
/* System.out.println("Case3: Bridge " + e.fingerprint + " has published an "
+ "extra info descriptor at " + e.published + " as relay, having a bandwidth "
+ "of " + totalBandwidth + ", but that was "
+ ((extraPubl - lastRelayPublBefore) / (24L * 60L * 60L * 1000L)) + " days = "
+ ((extraPubl - lastRelayPublBefore) / (60L * 1000L)) + " minutes = "
+ (extraPubl - lastRelayPublBefore) + " milliseconds before publishing one as "
+ "bridge. Not excluding."); */
                                    }
                                } else if (!bridgeRelayPublications.get(e.fingerprint).tailSet(extraPubl).isEmpty()) {
                                    long firstRelayPublAfter = bridgeRelayPublications.get(e.fingerprint).tailSet(extraPubl).first();
/* System.out.println("Case4: Bridge " + e.fingerprint + " has published an "
+ "extra info descriptor at " + e.published + " as relay, having a bandwidth "
+ "of " + totalBandwidth + ", but that was "
+ ((firstRelayPublAfter - extraPubl) / (24L * 60L * 60L * 1000L)) + " days = "
+ ((firstRelayPublAfter - extraPubl) / (60L * 1000L)) + " minutes = "
+ (firstRelayPublAfter - extraPubl) + " milliseconds after publishing one as "
+ "bridge. Not excluding as bridge."); */
                                } // (end of variant b)
                                //e.possiblyRelay = true; // variant c)
                            }
                            extrainfos.put(out, e);
                        }
                        digestBuilder.append(line + "\n");
                    }
                    br.close();
                    for (Map.Entry<String, Descriptor> descr : descriptors
                            .entrySet()) {
                        if (extrainfos.containsKey(
                                descr.getValue().extrainfoDigest)) {
                            descr.getValue().extrainfo = extrainfos.get(descr
                                    .getValue().extrainfoDigest);
                        }
                    }
                }
            }

            // write output files
            //if (currentTimestamp == -1)
            //    currentTimestamp = timestamp; // this is the first snapshot
            //else
            //    currentTimestamp += 1800000; // advance by 30 mins
            // if we are missing some snapshots, add NA's
            //while (currentTimestamp < timestamp) {
            //    runningOut.write(currentTimestamp + ",NA,NA\n");
            //    bandwidthOut.write(currentTimestamp + ",NA\n");
            //    geoipOut.write("" + currentTimestamp);
            //    for (int i = 0; i < allCountryCodes.size() - 1; i++)
            //        geoipOut.write(",NA");
            //    geoipOut.write("\n");
            //    currentTimestamp += 1800000; // advance by 30 mins
            //}

            // should we write previously parsed data to file?
            if (date == -1) {
                date = timestamp / (24 * 60 * 60 * 1000);
            }
            if (timestamp / (24 * 60 * 60 * 1000) > date) {
                runningOut.write(date + "," + (allTotals / timestampsSeen)
                        + "," + (allRunnings / timestampsSeen) + "\n");
                bandwidthOut.write(date + ","
                        + (allBandwidths / timestampsSeen) + ","
                        + (noRouterDesc / timestampsSeen) + ","
                        + (noExtraInfo / timestampsSeen) + ","
                        + (noHistory / timestampsSeen) + ","
                        + (noGeoip / timestampsSeen) + ","
                        + (allRunnings / timestampsSeen) + "\n");
                geoipDailyOut.write(date + ",");
                double totalClients = 0.0D;
                for (String cc : allCountryCodes) {
                    if (geoipDailyClients.containsKey(cc)) {
                        geoipDailyOut.write(String.format("%.2f,", geoipDailyClients.get(cc)));
                        totalClients += geoipDailyClients.get(cc);
                    } else {
                        geoipDailyOut.write("0,");
                    }
                }
                geoipDailyOut.write(String.format("%.2f%n", totalClients));
                geoipDailyClients.clear();
                date = timestamp / (24 * 60 * 60 * 1000);
                allTotals = 0;
                allRunnings = 0;
                timestampsSeen = 0;
                allBandwidths = 0;
                noRouterDesc = 0;
                noExtraInfo = 0;
                noHistory = 0;
                noGeoip = 0;
            }

            // should we write previously data for the last month to file?
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            cal.setTimeInMillis(timestamp);
            int currentMonth = cal.get(Calendar.MONTH);
            if (month == -1) {
                month = currentMonth;
            }
            if (currentMonth != month) {
                geoipOut.write(date + ",");
                double totalClients = 0.0D;
                for (String cc : allCountryCodes) {
                    if (geoipClients.containsKey(cc)) {
                        geoipOut.write(String.format("%.2f,", geoipClients.get(cc)));
                        totalClients += geoipClients.get(cc);
                    } else {
                        geoipOut.write("0,");
                    }
                }
                geoipOut.write(String.format("%.2f%n", totalClients));
                month = currentMonth;
                geoipClients.clear();
            }

            // add data for this snapshot
            int running = 0;
            int total = 0;
            long totalBandwidth = 0L;
            for (Router router : networkstatus.entries) {
                total++;
                if (router.running) {
                    running++;
                    if (router.descriptor != null) {
                        if (router.descriptor.extrainfo != null
                                && !router.descriptor.extrainfo.possiblyRelay) {
                            if (router.descriptor.extrainfo.writeHistory != null
                                    && router.descriptor.extrainfo.readHistory
                                           != null) {
                                String[] write1 = router.descriptor.extrainfo
                                        .writeHistory.split(" ");
                                String[] read1 = router.descriptor.extrainfo
                                        .readHistory.split(" ");
                                if (write1.length > 4 && read1.length > 4) {
                                    String[] write2 = write1[4].split(",");
                                    String[] read2 = read1[4].split(",");
                                    if (write2.length == read2.length) {
                                        long num = 0, sum = 0;
                                        for (int i = 0; i < write2.length;
                                                i++) {
                                            num++;
                                            sum += (Long.parseLong(write2[i])
                                                    + Long
                                                    .parseLong(read2[i])) / 2;
                                        }
                                        totalBandwidth += (sum / num);
                                    }
                                }
                            } else {
                                // either write or read history not found
                                noHistory++;
                            }
                            if (router.descriptor.extrainfo
                                    .geoipClientOrigins != null) {
                                if (router.descriptor.extrainfo
                                        .geoipClientOrigins.length() > 3) {
                                    String[] entries = router.descriptor
                                            .extrainfo.geoipClientOrigins
                                            .split(",");
                                    SimpleDateFormat logformat =
                                            new SimpleDateFormat(
                                            "yyyy-MM-dd HH:mm:ss");
                                    format.setTimeZone(TimeZone
                                            .getTimeZone("UTC"));
                                    long millisPublished = logformat.parse(
                                            router.descriptor.extrainfo
                                            .published,
                                            new ParsePosition(0)).getTime();
                                    long millisGeoipStart = logformat.parse(
                                            router.descriptor.extrainfo
                                            .geoipStartTime,
                                            new ParsePosition(0)).getTime();

                                    for (String ent : entries) {
                                        String countryCode =
                                                ent.substring(0, 2);
                                        int clients = Integer.parseInt(
                                                ent.substring(3));
                                        if (clients == 0) {
                                            continue;
                                        }
                                        double clientsPerHour =
                                                (double) (clients - 4)
                                                / (double) ((millisPublished
                                                - millisGeoipStart)
                                                / (30 * 60 * 1000));
                                        if (geoipClients.containsKey(
                                                countryCode)) {
                                            geoipClients.put(countryCode,
                                                    geoipClients.get(
                                                    countryCode)
                                                    + clientsPerHour);
                                        } else {
                                            geoipClients.put(countryCode,
                                                    clientsPerHour);
                                        }
                                        if (geoipDailyClients.containsKey(
                                                countryCode)) {
                                            geoipDailyClients.put(countryCode,
                                                    geoipDailyClients.get(
                                                    countryCode)
                                                    + clientsPerHour);
                                        } else {
                                            geoipDailyClients.put(countryCode,
                                                    clientsPerHour);
                                        }
                                    }
                                }
                            } else {
                                // no geoip information found
                                noGeoip++;
                            }
                        } else {
                            // no extra-info document found
                            noExtraInfo++;
                        }
                    } else {
                        // no router descriptor found
                        noRouterDesc++;
                    }
                }
            }
            allTotals += total;
            allRunnings += running;
            allBandwidths += totalBandwidth;
            timestampsSeen++;

            // periodically free snapshots that are older than 48 hours
            // (or rather: keep a maximum of 96 snapshots in memory)
            if (networkstatuses.size() > 144) {
                while (networkstatuses.size() > 96) {
                    networkstatuses.remove(networkstatuses.firstKey());
                }
                System.gc();
            }
        }
        runningOut.write(date + "," + (allTotals / timestampsSeen)
                + "," + (allRunnings / timestampsSeen) + "\n");
        bandwidthOut.write(date + "," + (allBandwidths / timestampsSeen) + ","
                + (noRouterDesc / timestampsSeen) + ","
                + (noExtraInfo / timestampsSeen) + ","
                + (noHistory / timestampsSeen) + ","
                + (noGeoip / timestampsSeen) + ","
                + (allRunnings / timestampsSeen) + "\n");
        geoipOut.write(date + ",");
        double totalClients = 0.0D;
        for (String cc : allCountryCodes) {
            if (geoipClients.containsKey(cc)) {
                geoipOut.write(String.format("%.2f,", geoipClients.get(cc)));
                totalClients += geoipClients.get(cc);
            } else {
                geoipOut.write("0,");
            }
        }
        geoipOut.write(String.format("%.2f%n", totalClients));
        geoipDailyOut.write(date + ",");
        totalClients = 0.0D;
        for (String cc : allCountryCodes) {
            if (geoipDailyClients.containsKey(cc)) {
                geoipDailyOut.write(String.format("%.2f,", geoipDailyClients.get(cc)));
                totalClients += geoipDailyClients.get(cc);
            } else {
                geoipDailyOut.write("0,");
            }
        }
        geoipDailyOut.write(String.format("%.2f%n", totalClients));
        runningOut.close();
        bandwidthOut.close();
        geoipOut.close();
        geoipDailyOut.close();
        System.out.println("Parsed all bridge files. "
                + ((System.currentTimeMillis() - started) / 1000)
                + " seconds");
    }
}

